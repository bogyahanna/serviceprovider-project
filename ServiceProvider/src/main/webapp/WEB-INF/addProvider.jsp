
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>Add provider data</title>

        <style type="text/css">
            h2  {text-align: center;
                 padding: 10px;}
            </style>
        </head>
        <body>
            <%@include file="headlineAndSearch.jsp" %>

            <h2>Please enter your Provider details:</h2>
            <div class="container">
            <form method="POST" action="ProviderServlet">
                <div class="form-group">
                    <label for="phone">Phone number:</label>
                    <input type="text" class="form-control" placeholder="Enter phone number" id="phone" name="phone">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <input type="text" class="form-control" placeholder="Enter address" id="address" name="address">
                </div>                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>
