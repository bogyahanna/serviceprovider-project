<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>Add new service</title>

        <style type="text/css">
            h2  {text-align: center;
                 padding: 10px;}
            </style>

        </head>
        <body>
            <%@include file="headlineAndSearch.jsp" %>

            <h2>Add new service</h2>
            <div class="container">
            <form method="POST" action="ServiceServlet" enctype="multipart/form-data" class="needs-validation" novalidate>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" placeholder="Enter name" id="name" name="name" required>
                    <div class="invalid-feedback">
                        A valid name is required.
                    </div>
                </div>
                <div class="form-group">
                    <label for="shortDescription">Short description:</label>
                    <input type="text" class="form-control" placeholder="Enter short description" id="shortDescription" name="shortDescription" required>
                    <div class="invalid-feedback">
                        Please enter a short description.
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea class="form-control" rows="7" style="height:25%;" placeholder="Enter description" id="description" name="description" required></textarea>
                    <div class="invalid-feedback">
                        Please enter a detailed description.
                    </div>
                </div>
                <div class="form-group">
                    <label for="image">Upload image:</label>
                    <input type="file" name="file">
                </div>
                <div class="form-group">
                    <label for="category">Category:</label>
                    <select id="category[]" name="category[]" class="form-control" multiple="multiple">                        
                        <c:forEach var="categoryDTO" items="${categoryDTOs}">
                            <option value="${categoryDTO.id}">${categoryDTO.name}</option>
                        </c:forEach>
                        <option value="0">Other</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <form method="POST" action="CategoryServlet">
                <div class="form-group invisible" id="new-category">
                    <label for="new-category">New category:</label>
                    <input type="text" class="form-control" placeholder="New category name" name="name" required>
                    <button type="submit" id="create-category" class="btn btn-primary">Create category</button>
                </div>
            </form>
            <c:if test="${addServiceError != null}">
                <div class="alert alert-danger" role="alert">
                    ${addServiceError}
                </div>
            </c:if>
            <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>           
            <script>
                const categorySelect = document.getElementById('category[]');
                const newCategoryDiv = document.getElementById('new-category');

                categorySelect.addEventListener('change', (event) => {
                    if (event.target.value === '0') {
                        newCategoryDiv.classList.remove('invisible');
                    } else {
                        newCategoryDiv.classList.add('invisible');
                    }
                });
            </script>

    </body>
</html>
