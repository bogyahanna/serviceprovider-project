<%@page import="java.util.List"%>
<%@page import="hu.braininghub.serviceprovider.model.MessageDTO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>        
        <title>Sent messages</title>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#accordion").accordion();
            });
        </script>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>

        <h3>Sent messages</h3>
        <div id="accordion">
            <c:forEach items="${messageDTOs}" var="message">
                <h1>
                    <table class="table table-borderless">
                        <tr>
                            <td>${message.timeOfSending}</td>
                            <td>  TO:    ${message.recipientName}</td>
                            <td>  SUBJECT:  ${message.subject}</td>
                        </tr>
                    </table>
                </h1>
                <div>
                    <p>    
                        ${message.message}
                    </p>
                    <p>
                    <form action="ListSentMessagesServlet" method="POST">
                        <button class="btn btn-outline-dark" id="deleteMessageButton" name="deleteMessageButton" value="${message.id}">Delete</button>
                    </form>
                </div>
            </c:forEach>
        </div>
    </body>
</html>
