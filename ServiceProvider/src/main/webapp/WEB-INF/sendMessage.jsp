<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>Send message</title>
    </head>
    <body>
         <%@include file="headlineAndSearch.jsp" %>
         <c:if test="${sendError != null}">
            <div class="alert alert-danger" role="alert">
                ${sendError}
            </div>
        </c:if>
        <h1>Send message</h1>
        <div class="container">
            <form method="POST" action="SendMessageServlet">
                <div class="form-group">
                    <label for="recipient">Recipient:</label>
                    <p>${recipentName}</p>
                </div>
                <div class="form-group">
                    <label for="subject">Subject: </label>
                    <input type="text" class="form-control" placeholder="Enter subject" id="subject" name="subject">
                </div>
                <div class="form-group">
                    <label for="message">Message:</label>
                    <textarea class="form-control" placeholder="Enter message" id="message" name="message" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </body>
</html>
