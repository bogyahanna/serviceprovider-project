<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>     
        <%@include file="head.jsp" %>        
        <%@include file="cssForHeadLine.jsp" %>
        <title>Card view</title>
        <%@include file="cssForCollapsible.jsp" %>        
    </head>
    <body>     
        <%@include file="headlineAndSearch.jsp" %>
        <br> <br>
        <div class="container">
            <div class="card-columns">
                <c:forEach items="${filteredList}" var="serviceDTO">
                    <div class="card bg-light mb-3" style="width: 17rem; border-color: #777;" name="service" id="service">
                        <img class="card-img-top" src=${serviceDTO.image} alt="Card image" style="object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title" style="font-family: serif; font-weight: bold;">${serviceDTO.name}</h4>
                            <p class="card-text">${serviceDTO.shortDescription}</p>
                            <button type="button" class="btn btn-outline-dark" id="button1">Details</button> 
                            <div class="content">
                                <p>
                                    <br>
                                    ${serviceDTO.description}
                                    <br>
                                    <c:forEach items="${serviceDTO.categoryDTOs}" var="categoryDTO">                               
                                    <form action="CategoryServlet" method="get">
                                        <button class="button button4" id="category" name="category" value="${categoryDTO.name}">#${categoryDTO.name}</button>  
                                    </form>
                                </c:forEach>
                                </p>
                            </div>
                            <br> <br>
                           <c:if test="${userDTO.email != serviceDTO.providerDTO.email}">
                            <form action="SendMessageServlet" method="get">
                                <button class="btn btn-outline-dark" id="providerId" name="providerId" value="${serviceDTO.providerDTO.id}">Interested</button>  
                            </form>
                        </c:if>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <%@include file="scriptForCollapsible.jsp" %>
    </body>
</html>