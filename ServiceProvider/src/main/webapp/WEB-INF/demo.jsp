<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>HOME</title>
        <%@include file="cssForBasicCard.jsp" %>
    </head>
    <body>      
        <%@include file="headlineAndSearch.jsp" %>
        <br> <br>
        <div class="container">
            <div class="card-columns">
                <c:forEach items="${serviceDTOs}" var="serviceDTO">
                    <div class="card bg-light mb-3" style="width: 17rem; border-color: #777;">
                        <img class="card-img-top" src=${serviceDTO.image} alt="Card image" style="object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title" style="font-weight: bold;">${serviceDTO.name}</h4>
                            <p class="card-text">${serviceDTO.shortDescription}</p>
                            <a href="LoginServlet" class="btn btn-outline-dark">Details</a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </body>
</html>
