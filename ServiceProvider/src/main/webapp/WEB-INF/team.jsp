<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>  
        <%--     <link rel="stylesheet" href="https://www.w3schools.com/html/styles.css">--%>
        <%@include file="head.jsp" %>
<!--        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <%@include file="cssForBasicCard.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
    </head>
    <body>      
        <%@include file="headlineAndSearch.jsp" %>
        <br> <br>
        <div class="container">
            <img src="images/team.jpg" width="500" class="mx-auto d-block" alt="team">
            <div class="d-flex justify-content-center align-items-center">
                    <h4>Our team</h4>
            </div>
        </div>
    </body>
</html>
