<%-- 
    Document   : noServices
    Created on : 2020.08.20., 11:18:42
    Author     : Kinga
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>   
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>No services</title>
        <style type="text/css">
            h6  {text-align: center;
                 padding: 10px;}
            </style>
    </head>
    <body> 
        <%@include file="headlineAndSearch.jsp" %>

        <div class="container">
                <br>
            <h6>There are no services</h6>
        </div>
    </body>
</html>

