package hu.braininghub.serviceprovider.helper;

import hu.braininghub.serviceprovider.model.UserDTO;
import java.util.Optional;
import javax.servlet.http.HttpSession;

public class SessionHelper {

    private static final String USER_SESSION_KEY = "userDTO";

    public static void setUserToSession(HttpSession session, UserDTO userDTO) {

        session.setAttribute(USER_SESSION_KEY, userDTO);
    }

    public static UserDTO getUserFromSession(HttpSession session) {

        return (UserDTO) session.getAttribute(USER_SESSION_KEY);
    }
    
    /*
    public static Optional<UserDTO> getUserFromSession(HttpSession session) {

        return Optional.ofNullable((UserDTO)session.getAttribute(USER_SESSION_KEY));
    }
    */
    
    public static void removeUserFromSession(HttpSession session) {

        session.removeAttribute(USER_SESSION_KEY);
    }
}