package hu.braininghub.serviceprovider.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import hu.braininghub.serviceprovider.entity.UserEntity;
import hu.braininghub.serviceprovider.helper.EmailValidateHelper;
import hu.braininghub.serviceprovider.mapper.UserMapper;
import hu.braininghub.serviceprovider.model.UserDAO;
import hu.braininghub.serviceprovider.model.UserDTO;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class LoginService {

    @Inject
    UserDAO dao;

    private UserEntity userEntity;

    public UserDTO userLogin(String email, String password, String validationCode) throws Exception {

        if (!EmailValidateHelper.isValidEmail(email)) {
            throw new Exception("Invalid email.");
        }

        if (!userExist(email)) {
            throw new Exception("Not registered user.");
        }

        if (!checkCredentials(password)) {
            throw new Exception("Password does not match for registered email.");
        }

        if (!userEntity.isValidated()) {
            if (!doValidate(validationCode)) {
                throw new Exception("Please enter validation code!");
            }
        }

        UserDTO user = UserMapper.MAPPER.toDTO(userEntity);

        return user;
    }

    private boolean userExist(String email) {

        userEntity = dao.getUser(email);

        return userEntity != null;
    }

    private boolean checkCredentials(String password) {
        BCrypt.Result verify = BCrypt.verifyer().verify(password.toCharArray(), userEntity.getPassword());

        return verify.verified;
    }

    private boolean doValidate(String validationCode) {
        if (validationCode.equals(userEntity.getValidationCode())) {
            dao.validateUser(userEntity.getEmail());
            return true;
        }
        else {
            return false;
        }
    }
}
