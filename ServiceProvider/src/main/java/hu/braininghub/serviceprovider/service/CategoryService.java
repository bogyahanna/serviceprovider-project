package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.mapper.CategoryMapper;
import hu.braininghub.serviceprovider.mapper.ServiceMapper;
import hu.braininghub.serviceprovider.model.CategoryDAO;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Stateless
public class CategoryService {

    @Inject
    CategoryDAO dao;

    public List<CategoryDTO> getCategories() throws SQLException {
        List<CategoryDTO> categoryDTOs = new ArrayList();

        for (CategoryEntity category : dao.getCategories()) {
            categoryDTOs.add(CategoryMapper.CATEGORY_MAPPER.toDTO(category));
        }
        return categoryDTOs;
    }

    public CategoryDTO getCategoryById(int id) {        
        return CategoryMapper.CATEGORY_MAPPER.toDTO(dao.getCategoryById(id));
    }

    public void addCategory(String name) {
        CategoryDTO dto = new CategoryDTO();
        dto.setName(name);
        dao.addNewCategory(CategoryMapper.CATEGORY_MAPPER.toEntity(dto));
    }
    
    public CategoryDTO getCategoryByName(String name) {
        return CategoryMapper.CATEGORY_MAPPER.toDTO(dao.getCategoryByName(name));
    }
    
     public List<ServiceDTO> categoryListing(CategoryDTO categoryDTO) throws SQLException{

        CategoryEntity category = CategoryMapper.CATEGORY_MAPPER.toEntity(categoryDTO);

        List<ServiceEntity> filteredServiceList = dao.categoryList(category);

        List<ServiceDTO> filteredSList = new ArrayList<>();

        for (ServiceEntity s : filteredServiceList) {
            filteredSList.add(ServiceMapper.SERVICE_MAPPER.toDTO(s));
        }

        return filteredSList;

    }

    public List<ServiceDTO> categoryListing(String clickedCategory) throws SQLException{

        List<ServiceEntity> filteredServiceList = dao.categoryList(clickedCategory);

        List<ServiceDTO> filteredSList = new ArrayList<>();

        for (ServiceEntity s : filteredServiceList) {
            filteredSList.add(ServiceMapper.SERVICE_MAPPER.toDTO(s));
        }

        return filteredSList;
    }
    
    public List<ServiceDTO> findAllServicesFromSameCategory(CategoryDTO dto) throws SQLException {
        CategoryEntity category = CategoryMapper.CATEGORY_MAPPER.toEntity(dto);
        
        List<ServiceEntity> filteredServices = dao.findAllServicesFromSameCategory(category);
        List<ServiceDTO> filteredServiceDTOs = new ArrayList<>();
        
        for(ServiceEntity s : filteredServices){
            filteredServiceDTOs.add(ServiceMapper.SERVICE_MAPPER.toDTO(s));
        }
        return filteredServiceDTOs;    
    }
    
}

