package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.mapper.MessageMapper;
import hu.braininghub.serviceprovider.model.MessageDAO;
import hu.braininghub.serviceprovider.model.MessageDTO;
import java.time.LocalDateTime;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class MessageService {

    @Inject
    MessageDAO dao;

    public void addMessage(LocalDateTime timeOfSending, String senderName,
            String sender, String recipientName, String recipient, String subject, String message) {

        MessageDTO dto = new MessageDTO();
        dto.setTimeOfSending(timeOfSending);
        dto.setSenderName(senderName);
        dto.setSender(sender);
        dto.setRecipientName(recipientName);
        dto.setRecipient(recipient);
        dto.setSubject(subject);
        dto.setMessage(message);

        dao.addMessage(MessageMapper.MESSAGE_MAPPER.toEntity(dto));
    }

    public MessageDTO getMessageById(Long id) {
        return MessageMapper.MESSAGE_MAPPER.toDTO(dao.getMessageById(id));
    }

    public void removeMessage(Long messageId) {
        dao.removeMessage(messageId);
    }

    public List<MessageDTO> getIncomingMessages(String recipient) {
        return MessageMapper.MESSAGE_MAPPER.toDTOList(dao.getIncomingMessages(recipient));
    }

    public List<MessageDTO> getSentMessages(String sender) {
        return MessageMapper.MESSAGE_MAPPER.toDTOList(dao.getSentMessages(sender));
    }
}
