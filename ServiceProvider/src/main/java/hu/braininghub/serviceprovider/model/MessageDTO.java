package hu.braininghub.serviceprovider.model;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MessageDTO {
    
    private Long id;
    private LocalDateTime timeOfSending;
    private String senderName;
    private String sender;
    private String recipientName;
    private String recipient;
    private String subject;
    private String message;
}
