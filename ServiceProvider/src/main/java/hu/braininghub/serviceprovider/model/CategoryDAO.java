package hu.braininghub.serviceprovider.model;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.LoggerFactory;

@Singleton
public class CategoryDAO {

    @PersistenceContext
    private EntityManager em;

    public CategoryEntity getCategoryById(int id) {
        try {
            CategoryEntity c = em.find(CategoryEntity.class, id);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Category successfully found by {} id", id);
            return c;
        } catch (NoResultException e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Category could not be found. Reason: {}", e.getMessage());
            return null;
        }
    }

    public List<CategoryEntity> getCategories() {
        List<CategoryEntity> categories = em.createQuery("SELECT c FROM CategoryEntity c").getResultList();

        return categories;
    }

    public void addNewCategory(CategoryEntity category) {
        try {
            em.persist(category);
            LoggerFactory.getLogger(this.getClass().getName()).debug("New category successfully created");
        } catch (Exception e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Error creating new category. Reason: {}", e.getMessage());
        }
    }

    public CategoryEntity getCategoryByName(String name) {
        try {
            TypedQuery<CategoryEntity> query = em.createQuery("SELECT c FROM CategoryEntity c WHERE c.name = :name", CategoryEntity.class);
            LoggerFactory.getLogger(this.getClass().getName()).debug("Category found by {} name", name);
            CategoryEntity category = query.setParameter("name", name).getSingleResult();
            return category;
        } catch (NoResultException e) {
            LoggerFactory.getLogger(this.getClass().getName()).error("Category could not be found. Reason: {}", e.getMessage());
            return null;
        }
    }

    public List<ServiceEntity> categoryList(CategoryEntity category) {

        List<ServiceEntity> serviceList = em.createQuery("SELECT s FROM ServiceEntity s").getResultList();
        List<CategoryEntity> categoryList = em.createQuery("SELECT c FROM CategoryEntity c").getResultList();

        List<ServiceEntity> filteredList = new ArrayList<>();

        serviceList.forEach(se -> {
            categoryList.stream().filter(ce -> (se.getCategories().contains(ce)))
                    .filter(ce -> {
                        return ce.getName().equals(category.getName());
                    })
                    .forEachOrdered(_item -> {
                        filteredList.add(se);
                    });
        });

        return filteredList;
    }

    public List<ServiceEntity> categoryList(String clickedCategory) {

        List<ServiceEntity> serviceList = em.createQuery("SELECT s FROM ServiceEntity s").getResultList();
        List<CategoryEntity> categoryList = em.createQuery("SELECT c FROM CategoryEntity c").getResultList();

        List<ServiceEntity> filteredList = new ArrayList<>();

        for (ServiceEntity se : serviceList) {
            for (CategoryEntity ce : categoryList) {
                if (se.getCategories().contains(ce)) {
                    if (ce.getName().equals(clickedCategory)) {
                        filteredList.add(se);
                    }
                }

            }

        }

        return filteredList;
    }

    public List<ServiceEntity> findAllServicesFromSameCategory(CategoryEntity category) {
               
        TypedQuery<ServiceEntity> query = em.createQuery(
                "SELECT DISTINCT s FROM CategoryEntity c LEFT JOIN c.services s "
                + "WHERE c = :category", ServiceEntity.class);
        query.setParameter("category", category);
        List<ServiceEntity> servicesWithTheSameCategory = query.getResultList();
        
        return servicesWithTheSameCategory;
    }
}
