
package hu.braininghub.serviceprovider.mapper;

import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.model.ServiceDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ProviderMapper.class, CategoryMapper.class})
public interface ServiceMapper {

    ServiceMapper SERVICE_MAPPER = Mappers.getMapper(ServiceMapper.class);    
    
    @Mapping(source = "service.provider", target = "providerDTO")
    @Mapping(source = "service.categories", target = "categoryDTOs")
    ServiceDTO toDTO(ServiceEntity service);

    @Mapping(source = "serviceDTO.providerDTO", target = "provider")
    @Mapping(source = "serviceDTO.categoryDTOs", target = "categories")
    ServiceEntity toEntity(ServiceDTO serviceDTO);
}
