
package hu.braininghub.serviceprovider.mapper;

import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.model.AnonymServiceDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Johanna
 */
@Mapper
public interface AnonymServiceMapper {
    
    AnonymServiceMapper INSTANCE = Mappers.getMapper(AnonymServiceMapper.class);

    AnonymServiceDTO toAnonymDTO(ServiceEntity service);

    List<AnonymServiceDTO> toAnonymDTOList(List<ServiceEntity> services);
}
