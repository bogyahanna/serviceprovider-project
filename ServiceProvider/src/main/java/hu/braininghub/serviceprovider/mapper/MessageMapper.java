package hu.braininghub.serviceprovider.mapper;

import hu.braininghub.serviceprovider.entity.MessageEntity;
import hu.braininghub.serviceprovider.model.MessageDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageMapper {
    
    MessageMapper MESSAGE_MAPPER = Mappers.getMapper(MessageMapper.class);

    MessageDTO toDTO (MessageEntity message);
    
    MessageEntity toEntity (MessageDTO dto);
    
    List<MessageDTO> toDTOList (List<MessageEntity> message);
}
