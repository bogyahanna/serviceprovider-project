/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.serviceprovider.mapper;

import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProviderMapper {

    ProviderMapper PROVIDER_MAPPER = Mappers.getMapper(ProviderMapper.class);

    ProviderDTO toDTO(ProviderEntity provider);

    ProviderEntity toEntity(ProviderDTO providerDTO);
}
