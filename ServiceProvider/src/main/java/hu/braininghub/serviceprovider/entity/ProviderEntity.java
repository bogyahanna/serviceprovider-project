package hu.braininghub.serviceprovider.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "provider")
public class ProviderEntity implements Serializable {
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Column
    private String email;
    
    @Column
    @EqualsAndHashCode.Exclude
    private String phone;
    
    @Column
    @EqualsAndHashCode.Exclude
    private String address;
    
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "provider", fetch = FetchType.EAGER)
    private List<ServiceEntity> services;
    
    
    
}
