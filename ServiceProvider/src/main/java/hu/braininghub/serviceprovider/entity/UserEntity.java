package hu.braininghub.serviceprovider.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@EntityListeners(UserEntityListener.class)
@Table (name = "users")
public class UserEntity implements Serializable {
    
    @Id
    private String email;
       
    @Column
    private String password;    
    
    @Column
    private String name;
    
    @Column
    private Date timeOfRegister;
    
    @Column
    private boolean isValidated;
    
    @Column
    private String validationCode;
}
