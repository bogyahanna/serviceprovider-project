package hu.braininghub.serviceprovider.entity;

import hu.braininghub.serviceprovider.service.EmailService;
import javax.inject.Inject;
import javax.persistence.*;
import org.slf4j.LoggerFactory;

public class UserEntityListener {
    
    @Inject
    EmailService notifyService;
    
    @PostPersist
    void onPostPersist(UserEntity entity) {
        LoggerFactory.getLogger(this.getClass().getName()).debug("User created with Email: {} and Name: {}", entity.getEmail(), entity.getName());
    }
    
    @PostUpdate
    void onPostUpdate(UserEntity entity) {
        LoggerFactory.getLogger(this.getClass().getName()).warn("User with {} was updated.", entity.getEmail());
    }
}
