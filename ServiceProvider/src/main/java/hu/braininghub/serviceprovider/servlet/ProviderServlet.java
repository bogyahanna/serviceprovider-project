package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.ProviderService;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ProviderServlet", urlPatterns = {"/ProviderServlet"})
public class ProviderServlet extends HttpServlet {

    @Inject
    private ServiceService serviceService;

    @Inject
    private ProviderService providerService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO loggedInUser = SessionHelper.getUserFromSession(request.getSession());

        if (loggedInUser != null) {
            request.getRequestDispatcher("WEB-INF/addProvider.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String phone = request.getParameter("phone");
        String address = request.getParameter("address");

        UserDTO loggedInUser = SessionHelper.getUserFromSession(request.getSession());
        String email = loggedInUser.getEmail();

        providerService.addProvider(email, phone, address);

        response.sendRedirect(request.getContextPath() + "/ServiceServlet");

    }

}
