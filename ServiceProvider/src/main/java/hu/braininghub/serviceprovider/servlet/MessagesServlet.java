package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.MessageDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.MessageService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MessagesServlet", urlPatterns = {"/MessagesServlet"})
public class MessagesServlet extends HttpServlet {

    @Inject
    private MessageService messageService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {
            
            try {
                List<MessageDTO> incomingMessageDTOs = messageService.getIncomingMessages(user.getEmail());
                request.setAttribute("incomingMessageDTOs", incomingMessageDTOs);

                List<MessageDTO> sentMessageDTOs = messageService.getSentMessages(user.getEmail());
                request.setAttribute("sentMessageDTOs", sentMessageDTOs);
            } catch (Exception e) {
                request.setAttribute("listMessageError", e.getMessage());
            }
            request.getRequestDispatcher("WEB-INF/listMessages.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {

            if (request.getParameter("deleteIncomingMessageButton") != null) {
                try {
                    Long messageId = Long.parseLong(request.getParameter("deleteIncomingMessageButton"));
                    if (messageService.getMessageById(messageId).getRecipient().equals(user.getEmail())) {
                        messageService.removeMessage(messageId);
                    }
                } catch (Exception ex) {
                    request.setAttribute("deleteMessageError", ex.getMessage());
                }
            }

            if (request.getParameter("deleteSentMessageButton") != null) {
                try {
                    Long messageId = Long.parseLong(request.getParameter("deleteSentMessageButton"));
                    if (messageService.getMessageById(messageId).getSender().equals(user.getEmail())) {
                        messageService.removeMessage(messageId);
                    }
                } catch (Exception ex) {
                    request.setAttribute("deleteMessageError", ex.getMessage());
                }
            }

            response.sendRedirect(request.getContextPath() + "/MessagesServlet");

        } else {

            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }
}
