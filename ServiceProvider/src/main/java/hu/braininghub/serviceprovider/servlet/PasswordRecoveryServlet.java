package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.UserService;
import java.io.IOException;
import java.util.Enumeration;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * @author Lej
 */
@WebServlet(name = "PasswordRecoveryServlet", urlPatterns = {"/PasswordRecoveryServlet"})
public class PasswordRecoveryServlet extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDTO user = SessionHelper.getUserFromSession(request.getSession());
        
        if (user != null) {
            request.getRequestDispatcher("WEB-INF/changePwd.jsp").forward(request, response);
        } else {
            request.setAttribute("emailAttempt", request.getParameter("loginAttempt"));
            request.getRequestDispatcher("WEB-INF/pwdRecover.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Enumeration<String> parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements()) {

            String nextElement = parameterNames.nextElement();

            if (nextElement == null) {
                response.sendRedirect(request.getContextPath() + "/PasswordRecoveryServlet");
                break;
            }

            if (nextElement.isEmpty()) {
                response.sendRedirect(request.getContextPath() + "/PasswordRecoveryServlet");
                break;
            }
        }

        if (request.getParameter("email") == null && !request.getParameter("newPassword").equals(request.getParameter("confirm_newPassword"))) {
            request.setAttribute("newPwdError", "Passwords do not match.");
            request.getRequestDispatcher("WEB-INF/changePwd.jsp").forward(request, response);
            return;
        }
        
        request.setAttribute("emailAttempt", request.getParameter("email"));

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {
            try {
                userService.changePassword(user.getEmail(), request.getParameter("newPassword"));
                response.sendRedirect(request.getContextPath() + "/LogoutServlet");
            } catch (Exception ex) {
                request.setAttribute("newPwdError", ex.getMessage());
                request.getRequestDispatcher("WEB-INF/changePwd.jsp").forward(request, response);
            }
        } else {
            try {
                userService.recoverPassword(request.getParameter("email"));
                response.sendRedirect(request.getContextPath() + "/LoginServlet");
            } catch (Exception ex) {
                request.setAttribute("newPwdError", ex.getMessage());
                request.getRequestDispatcher("WEB-INF/pwdRecover.jsp").forward(request, response);
            }
        }
    }
}
