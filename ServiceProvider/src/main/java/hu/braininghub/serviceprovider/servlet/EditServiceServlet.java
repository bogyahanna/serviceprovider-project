package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.helper.UploadHelper;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.CategoryService;
import hu.braininghub.serviceprovider.service.ProviderService;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.slf4j.LoggerFactory;

@WebServlet(name = "EditServiceServlet", urlPatterns = {"/EditServiceServlet"})
@MultipartConfig
public class EditServiceServlet extends HttpServlet {

    @Inject
    private ServiceService serviceService;

    @Inject
    private CategoryService categoryService;

    @Inject
    private ProviderService providerService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserDTO loggedInUser = SessionHelper.getUserFromSession(session);
        final String id = request.getParameter("id");

        if (loggedInUser == null) {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        } else if (id == null) {
            response.sendRedirect(request.getContextPath() + "/ListServicesServlet");
        } else {
            int serviceId;
            try {
                serviceId = Integer.parseInt(id);
            } catch (NumberFormatException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("A valid serviceId is needed!" + ex.getMessage());
                session.setAttribute("serviceError", "Invalid service! Please enter a valid serviceId from your own services!");
                response.sendRedirect(request.getContextPath() + "/ListServicesServlet");
                return;
            }
            ServiceDTO serviceToBeEdited = serviceService.getServiceById(serviceId);
            ProviderDTO providerDTO = null;
            try {
                providerDTO = serviceToBeEdited.getProviderDTO();

            } catch (NullPointerException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("A valid serviceId is needed");
            }

            if (providerDTO == null) {
                LoggerFactory.getLogger(this.getClass().getName()).error("A valid serviceId is needed");
                session.setAttribute("serviceError", "Invalid service! Please enter a valid serviceId from your own services!");
                response.sendRedirect(request.getContextPath() + "/ListServicesServlet");
            } else if (!providerDTO.getEmail().equals(loggedInUser.getEmail())) {
                LoggerFactory.getLogger(this.getClass().getName()).error("A valid serviceId is needed");
                session.setAttribute("serviceError", "Invalid service! Please enter a valid serviceId from your own services!");
                response.sendRedirect(request.getContextPath() + "/ListServicesServlet");

            } else {
                try {
                    session.setAttribute("id", serviceId);
                    request.setAttribute("serviceDTO", serviceToBeEdited);
                    request.setAttribute("categoryDTOs", categoryService.getCategories());

                    request.getRequestDispatcher("WEB-INF/edit.jsp").forward(request, response);
                } catch (IOException | SQLException | ServletException ex) {
                    LoggerFactory.getLogger(this.getClass().getName()).error("Error occured while editing a service");
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Enumeration<String> parameterNames = request.getParameterNames();

        try {

            while (parameterNames.hasMoreElements()) {

                String nextElement = parameterNames.nextElement();

                if (nextElement == null) {
                    request.setAttribute("addServiceError", nextElement + " cannot be NULL.");
                    request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                    break;
                }

                if (nextElement.isEmpty()) {
                    request.setAttribute("addServiceError", nextElement + " cannot be EMPTY.");
                    request.getRequestDispatcher("WEB-INF/register.jsp").forward(request, response);
                    break;
                }
            }

            String name = request.getParameter("name");
            String shortDescription = request.getParameter("shortDescription");
            String description = request.getParameter("description");

            final String realPath = getServletContext().getRealPath("/");
            Part filePart = request.getPart("file");

            String pathToPersist = "";

            if (!filePart.getSubmittedFileName().equals("")) {

                UploadHelper uploadHelper = new UploadHelper();

                pathToPersist = uploadHelper.uploadFile(realPath, filePart);
            }

            List<CategoryDTO> categoryDTOs = new ArrayList<>();

            String[] categories = request.getParameterValues("category[]");
            for (String category : categories) {
                CategoryDTO categoryDTO = categoryService.getCategoryById(Integer.valueOf(category));
                categoryDTOs.add(categoryDTO);
            }

            HttpSession session = request.getSession();
            UserDTO loggedInUser = SessionHelper.getUserFromSession(session);
            int serviceId = (Integer) session.getAttribute("id");
            String providerEmail = loggedInUser.getEmail();
            ProviderDTO providerDTO = providerService.getProviderByEmail(providerEmail);

            ServiceDTO dto = serviceService.getServiceById(serviceId);
            serviceService.updateService(dto, name, shortDescription, description, pathToPersist, categoryDTOs, providerDTO);

        } catch (IOException | NumberFormatException | ServletException ex) {

            LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred");
            request.setAttribute("editError", ex.getMessage());
            request.getRequestDispatcher("WEB-INF/edit.jsp").forward(request, response);
            return;
        }
        request.removeAttribute("editError");
        response.sendRedirect("ListServicesServlet");
    }
}
