package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.MessageDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.MessageService;
import hu.braininghub.serviceprovider.service.ProviderService;
import hu.braininghub.serviceprovider.service.UserService;
import java.io.IOException;
import java.time.LocalDateTime;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "SendMessageServlet", urlPatterns = {"/SendMessageServlet"})
public class SendMessageServlet extends HttpServlet {

    @Inject
    private MessageService messageService;

    @Inject
    private ProviderService providerService;

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());

        if (user != null) {

            try {
                if (request.getParameter("messageid") != null) {
                    Long messageId = Long.parseLong(request.getParameter("messageid"));
                    MessageDTO message = messageService.getMessageById(messageId);
                    if (message == null) {
                        request.setAttribute("sendError", "Invalid recipent!");
                        request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                        return;
                    }
                    request.getSession().setAttribute("messageid", messageId);
                    String recipientName = message.getSenderName();
                    request.getSession().setAttribute("recipentName", recipientName);
                    request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                    return;
                } else if (request.getParameter("providerId") == null) {
                    request.setAttribute("sendError", "Invalid recipent!");
                    request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                    return;
                }
            } catch (Exception e) {
                request.setAttribute("sendError", e.getMessage());
                request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                return;
            }

            int currentProviderId;

            try {
                currentProviderId = Integer.parseInt(request.getParameter("providerId"));
            } catch (NumberFormatException nfe) {
                request.setAttribute("sendError", "Invalid recipent!");
                request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                return;
            }

            // get recipent here
            try {

                ProviderDTO provider = providerService.getProviderById(currentProviderId);
                if (provider == null) {
                    request.setAttribute("sendError", "Provider not available!");
                    request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
                    return;
                }

                // set recipent here
                String recipentName = userService.getUserName(provider.getEmail());
                request.getSession().setAttribute("recipentName", recipentName);
                request.getSession().setAttribute("recipentEmail", provider.getEmail());
                request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);

            } catch (Exception ex) {
                request.setAttribute("sendError", ex.getMessage());
                request.getRequestDispatcher("WEB-INF/sendMessage.jsp").forward(request, response);
            }

        } else {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserDTO user = SessionHelper.getUserFromSession(session);

        if (user != null) {

            if (request.getSession().getAttribute("messageid") != null) {

                Long messageId = Long.valueOf(String.valueOf(request.getSession().getAttribute("messageid")));

                MessageDTO message = messageService.getMessageById(messageId);
                messageService.addMessage(LocalDateTime.now(), message.getRecipientName(), message.getRecipient(), message.getSenderName(), message.getSender(), request.getParameter("subject"), request.getParameter("message"));
                session.removeAttribute("messageid");
                response.sendRedirect(request.getContextPath() + "/MessagesServlet");
            } else {
                LocalDateTime timeOfSending = LocalDateTime.now();
                String senderName = user.getName();
                String sender = user.getEmail();

                // foolproof this
                String recipientName = (String) request.getSession().getAttribute("recipentName");
                String recipientEmail = (String) request.getSession().getAttribute("recipentEmail");
                String subject = request.getParameter("subject");
                String message = request.getParameter("message");

                // try
                messageService.addMessage(timeOfSending, senderName, sender, recipientName, recipientEmail, subject, message);
                // on successful persist
                request.getSession().removeAttribute("recipentName");
                request.getSession().removeAttribute("recipentEmail");
                // catch

                response.sendRedirect(request.getContextPath() + "/MessagesServlet");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/LoginServlet");
        }
    }
}
