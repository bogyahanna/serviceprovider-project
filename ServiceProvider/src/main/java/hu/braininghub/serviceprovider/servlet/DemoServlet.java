package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.AnonymServiceDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.LoggerFactory;

@WebServlet(name = "DemoServlet", urlPatterns = {"/DemoServlet"})
public class DemoServlet extends HttpServlet {

    @Inject
    private ServiceService serviceService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserDTO user = SessionHelper.getUserFromSession(session);
        List<AnonymServiceDTO> anonymServiceDTOs = null;
        List<ServiceDTO> serviceDTOs = null;

        if (user == null) {
            try {
                anonymServiceDTOs = serviceService.getAnonymServices();
                for (AnonymServiceDTO dto : anonymServiceDTOs) {
                    if (dto.getImage().equals("")) {
                        dto.setImage("images/placeholder.jpg");
                    }
                }
                session.setAttribute("serviceDTOs", anonymServiceDTOs);
            } catch (SQLException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred");
            }
            request.getRequestDispatcher("WEB-INF/demo.jsp").forward(request, response);

        } else {
            try {
                serviceDTOs = serviceService.getServices();
                for (ServiceDTO dto : serviceDTOs) {
                    if (dto.getImage().equals("")) {
                        dto.setImage("images/placeholder.jpg");
                    }
                }
                session.setAttribute("serviceDTOs", serviceDTOs);
                request.setAttribute("categoryError", session.getAttribute("categoryError"));
                session.removeAttribute("categoryError");

            } catch (SQLException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred");
            }
            request.getRequestDispatcher("WEB-INF/collapsibleCard.jsp").forward(request, response);

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
