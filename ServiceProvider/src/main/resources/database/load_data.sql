insert into users(email, name, password, isvalidated, timeofregister, validationcode) values ('notemail@email.hu', 'name1', '$2y$12$9caGNHBRABLePcJDl1/u/eK3OevPNFGVLZL3e.WSwkDBop0qtBRNm', false, '2020-08-01', 'random');
insert into users(email, name, password, isvalidated, timeofregister, validationcode) values ('neitheremail@email.hu', 'name1', '$2y$12$9caGNHBRABLePcJDl1/u/eK3OevPNFGVLZL3e.WSwkDBop0qtBRNm', false, '2020-09-01', 'random');
insert into users(email, name, password, isvalidated, timeofregister, validationcode) values ('email@email.hu', 'name1', '$2y$12$9caGNHBRABLePcJDl1/u/eK3OevPNFGVLZL3e.WSwkDBop0qtBRNm', true, '2020-08-01', 'random');
insert into users(email, name, password, isvalidated, timeofregister, validationcode) values ('team.pktp@gmail.com', 'Elek Teszt', '$2y$12$9caGNHBRABLePcJDl1/u/eK3OevPNFGVLZL3e.WSwkDBop0qtBRNm', true, '2020-08-01', 'random');

insert into category(id, name) values (-11, 'health');
insert into category(id, name) values (-10, 'security');
insert into category(id, name) values (-9, 'human resources');
insert into category(id, name) values (-12, 'beauty');
insert into category(id, name) values (-13, 'IT');
insert into category(id, name) values (-14, 'alpinism');
insert into category(id, name) values (-15, 'industries');

insert into provider(id, address, email, phone) values (-11, 'Budapest', 'email@email.hu', '6445');
insert into provider(id, address, email, phone) values (-10, 'Tátra utca 48', 'team.pktp@gmail.com', '06301807072');
insert into provider(id, address, email, phone) values (-9, 'Tomori utca 14', 'provider2@email.hu', '06301807071');

INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-19, 'As an adult, you are not immune to dental problems. In addition to maintaining a good home care routine, the best thing you can do is to schedule regular dental checkups and professional cleanings.', 'https://i-cf3.gskstatic.com/content/dam/cf-consumer-healthcare/paradontax/master/assets/caringforgums/dentist-treating-patient.jpg?auto=format', 'Stomatology', 'Stomatology is the study of the structure, function and diseases of the oral cavity.', -11);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-16, 'Human resources is the set of the people who make up the workforce of an organization, business sector, industry, or economy. A narrower concept is human capital, the knowledge which the individuals embody.', 'https://bossofcloud.com/wp-content/uploads/2020/02/HR-Tools-1080x675.jpg', 'HR Specialist', 'Human resource managers are in charge of every aspect of the employee life cycle in an organization', -10);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-17, 'A bodyguard looks out for the personal security of individuals such as political figures, famous celebrities, business executives, or other individuals who may be in danger of personal attacks.', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR74vtMYmDQyQaAUL74KkQUwGNmS16pc0WQ4Q&usqp=CAU','Bodyguard', 'A bodyguard is an individual who protects another from harm or threats.', -9);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-18, 'Home security includes both the security hardware placed on a property and individuals personal security practices. Security hardware includes doors, locks, alarm systems, lighting, motion detectors, and security camera systems.', 'https://5.imimg.com/data5/RX/NW/MY-33055175/electronic-security-system-250x250.jpg' , 'Security System', 'According to an FBI report, 58.3% of burglaries in the United States involved forcible entry.', -9);

INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-10, 'In a world run on technology, IT specialists, also known as an information technology specialist, are what every organization can never have enough of.', 'https://i.imgur.com/VClM6ob.jpg', 'IT Specialist', 'It is responsible for the implementation, monitoring, and maintenance of IT systems', -11);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-11, 'Hair technicians provide various services such as shampooing, conditioning, cutting, styling, dying, treatments, facial hair removal, and wig management.', 'https://www.spamiamibeach.com/files/miami-south-beach-hair-extensions-best-salon.jpg', 'Hair Technician', 'Specialized in performing hair care and are responsible for cutting, dying and styling hair', -10);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-12, 'The need for industrial alpinism keeps growing every year. The current day safety equipment provides possibilities to avoid high costs and long time of safety railings and scaffoldings by applying industrial professional climbers.', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTyFFUjcnl9tGa8LUyON0TpDLyVcJR1Kbcn4A&usqp=CAU','Industrial Alpinist', 'Industrial dust, gases and other factors have a negative impact on the cover of the surfaces and structures.', -9);
INSERT INTO service(ID, DESCRIPTION, IMAGE, NAME, SHORTDESCRIPTION, provider_id) VALUES (-13, 'Medicinal chemistry involves the identification, synthesis and development of new chemical entities suitable for therapeutic use. It also includes the study of existing drugs, and their biological properties.', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSm8uTvAmn_b1nUFrXVaM6Vxj18oaQxe1kagw&usqp=CAU' , 'Medicinal Chemist', 'Medicinal chemistry is the science involved with designing, synthesizing and developing pharmaceutical drugs. ', -9);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-9, -16);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-13, -10);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-10, -10);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-9, -10);


INSERT INTO category_service_jt (category_id, service_id) VALUES (-11, -17);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-10, -17);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-13, -18);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-10, -18);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-11, -19);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-12, -19);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-11, -11);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-12, -11);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-14, -12);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-15, -12);

INSERT INTO category_service_jt (category_id, service_id) VALUES (-11, -13);
INSERT INTO category_service_jt (category_id, service_id) VALUES (-13, -13);

insert into messages(id, message, recipient, sender, subject, timeOfSending, senderName,recipientName) values (-11, 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls','email@email.hu','email@email.hu','test data','2020-01-01 10:00:00','Joe','Joe');
insert into messages(id, message, recipient, sender, subject, timeOfSending, senderName,recipientName) values (-12, 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly','team.pktp@gmail.com','email@email.hu','test data2','2010-01-01 10:00:00','John','Jane');
insert into messages(id, message, recipient, sender, subject, timeOfSending, senderName,recipientName) values (-13, 'The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz.','team.pktp@gmail.com','team.pktp@gmail.com','test data3','2000-01-01 10:00:00','Jane','Jane');
insert into messages(id, message, recipient, sender, subject, timeOfSending, senderName,recipientName) values (-14, 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth','email@email.hu','team.pktp@gmail.com','test data4','2002-01-01 10:00:00','Jack','Joe');

