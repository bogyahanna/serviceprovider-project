package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.model.ProviderDAO;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProviderServiceTest {

    @Mock
    ProviderDAO dao;

    ProviderService service;

    public ProviderServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        dao = Mockito.mock(ProviderDAO.class);
        service = new ProviderService(dao);
        
        ProviderEntity p1 = new ProviderEntity(1, "email@email.hu", "06050506", "Budapest", null);
        ProviderEntity p2 = new ProviderEntity(2, "nomail@email.hu", "06050507", "Buda", null);
        ProviderEntity p3 = new ProviderEntity(3, "eachmail@email.hu", "06050508", "Pest", null);
        
        List<ProviderEntity> providers = new ArrayList<>();
        providers.add(p1);
        providers.add(p2);
        providers.add(p3);
        
        when(dao.getProviders()).thenReturn(providers);
    }

    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testFindAll(){                
        assertEquals(dao.getProviders().size(), service.getProviders().size());
    }

}
