package hu.braininghub.serviceprovider.service;

import hu.braininghub.serviceprovider.entity.CategoryEntity;
import hu.braininghub.serviceprovider.entity.ProviderEntity;
import hu.braininghub.serviceprovider.entity.ServiceEntity;
import hu.braininghub.serviceprovider.model.CategoryDTO;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.model.ServiceDAO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ServiceServiceTest {
    
    @Mock
    ServiceDAO serviceDAO;
    
    ServiceService serviceService;
    
    public ServiceServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        serviceDAO = Mockito.mock(ServiceDAO.class);
        serviceService = new ServiceService(serviceDAO);
    }
    
    @AfterEach
    public void tearDown() {
    }

    
    @Test
    public void getServices_mappingSuccessful() {
        
        List<ServiceEntity> services = new ArrayList<>();
        
        ServiceEntity s1 = new ServiceEntity();
        s1.setId(7);
        s1.setName("service1");
        s1.setShortDescription("short description1");
        s1.setDescription("longer description1");
        s1.setImage("images/cica.jpg");
        
        ProviderEntity p1 = new ProviderEntity();
        p1.setId(7);
        p1.setAddress("address1");
        p1.setEmail("provider1@email.hu");
        p1.setPhone("1111111");
        s1.setProvider(p1);
        
        CategoryEntity c1 = new CategoryEntity();
        c1.setId(7);
        c1.setName("category1");
        CategoryEntity c2 = new CategoryEntity();
        c2.setId(8);
        c2.setName("category2");
        CategoryEntity c3 = new CategoryEntity();
        c3.setId(9);
        c3.setName("category3");
        
        List<CategoryEntity> categories1 = new ArrayList<>();
        categories1.add(c1);
        categories1.add(c2);
        categories1.add(c3);

        s1.setCategories(categories1);
        
        services.add(s1);
        
        ServiceEntity s2 = new ServiceEntity();
        s2.setId(8);
        s2.setName("service2");
        s2.setShortDescription("short description2");
        s2.setDescription("longer description2");
        s2.setImage("images/cica.jpg");
        s2.setProvider(p1);
        
        List<CategoryEntity> categories2 = new ArrayList<>();
        categories2.add(c2);
        categories2.add(c3);

        s2.setCategories(categories2);
    
        services.add(s2);
        
        List<ServiceDTO> serviceDTOs = new ArrayList<>();
        
        ServiceDTO s3 = new ServiceDTO();
        s3.setId(7);
        s3.setName("service1");
        s3.setShortDescription("short description1");
        s3.setDescription("longer description1");
        s3.setImage("images/cica.jpg");
        
        ProviderDTO p2 = new ProviderDTO();
        p2.setId(7);
        p2.setAddress("address1");
        p2.setEmail("provider1@email.hu");
        p2.setPhone("1111111");
        s3.setProviderDTO(p2);
        
        CategoryDTO c4 = new CategoryDTO();
        c4.setId(7);
        c4.setName("category1");
        CategoryDTO c5 = new CategoryDTO();
        c5.setId(8);
        c5.setName("category2");
        CategoryDTO c6 = new CategoryDTO();
        c6.setId(9);
        c6.setName("category3");
        
        List<CategoryDTO> categoryDTOs = new ArrayList<>();
        categoryDTOs.add(c4);
        categoryDTOs.add(c5);
        categoryDTOs.add(c6);

        s3.setCategoryDTOs(categoryDTOs);
        
        serviceDTOs.add(s3);
        
        ServiceDTO s4 = new ServiceDTO();
        s4.setId(8);
        s4.setName("service2");
        s4.setShortDescription("short description2");
        s4.setDescription("longer description2");
        s4.setImage("images/cica.jpg");
        s4.setProviderDTO(p2);
        
        List<CategoryDTO> categoryDTOs2 = new ArrayList<>();
        categoryDTOs2.add(c5);
        categoryDTOs2.add(c6);

        s4.setCategoryDTOs(categoryDTOs2);
    
        serviceDTOs.add(s4);
        
        when(serviceDAO.getServices()).thenReturn(services);
        
        List<ServiceDTO> serviceDTOs1 = null;
        
        try {
            serviceDTOs1 = serviceService.getServices();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(int i = 0; i < serviceDTOs.size(); i++){
            assertEquals(serviceDTOs.get(i), serviceDTOs1.get(i));
        }
        
    }
    
    @Test
    public void getServiceById_withId1_returnsService() {
        
        ProviderEntity p1 = new ProviderEntity();
        p1.setId(7);
        p1.setAddress("address1");
        p1.setEmail("provider1@email.hu");
        p1.setPhone("1111111");
        
        CategoryEntity c1 = new CategoryEntity();
        c1.setId(7);
        c1.setName("category1");
        CategoryEntity c2 = new CategoryEntity();
        c2.setId(8);
        c2.setName("category2");
        CategoryEntity c3 = new CategoryEntity();
        c3.setId(9);
        c3.setName("category3");
        
        List<CategoryEntity> categories1 = new ArrayList<>();
        categories1.add(c1);
        categories1.add(c2);
        categories1.add(c3);
        
        ProviderDTO p2 = new ProviderDTO();
        p2.setId(7);
        p2.setAddress("address1");
        p2.setEmail("provider1@email.hu");
        p2.setPhone("1111111");
        
        CategoryDTO c4 = new CategoryDTO();
        c4.setId(7);
        c4.setName("category1");
        CategoryDTO c5 = new CategoryDTO();
        c5.setId(8);
        c5.setName("category2");
        CategoryDTO c6 = new CategoryDTO();
        c6.setId(9);
        c6.setName("category3");
        
        List<CategoryDTO> categoryDTOs1 = new ArrayList<>();
        categoryDTOs1.add(c4);
        categoryDTOs1.add(c5);
        categoryDTOs1.add(c6);
        
        ServiceDTO dto = new ServiceDTO(1, "service1", "short descr1", "longer descr1", "images/cica.jpg", p2, categoryDTOs1);
        
        when(serviceDAO.getServiceById(1)).thenReturn(new ServiceEntity(1, "service1", "short descr1", "longer descr1", "images/cica.jpg", p1, categories1));

        final ServiceDTO serviceDTO = serviceService.getServiceById(1);
        
        assertEquals(serviceDTO, dto);

    }
    

}
